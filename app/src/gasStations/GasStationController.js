(function(){

  angular
       .module('gasStations')
       .controller('GasStationController', [
          'gasStationService', '$mdSidenav', '$mdBottomSheet', '$log', '$q',
          GasStationController
       ]);

  /**
   * Main Controller for the Angular Material Starter App
   * @param $scope
   * @param $mdSidenav
   * @param avatarsService
   * @constructor
   */
  function GasStationController( gasStationService, $mdSidenav, $mdBottomSheet, $log, $q, $timeout) {
    var self = this;

    self.selected     = null;
    self.gasStations        = [ ];
    self.selectGasStation   = selectGasStation;
    self.toggleList   = toggleGasStationsList;
    self.makeContact  = makeContact;

    // Load all registered gasStations

    gasStationService
          .loadAllGasStations()
          .then( function( gasStations ) {
            self.gasStations    = [].concat(gasStations);
          });

    // *********************************
    // Internal methods
    // *********************************

    /**
     * Hide or Show the 'left' sideNav area
     */
    function toggleGasStationsList() {
      $mdSidenav('left').toggle();
    }

    /**
     * Select the current avatars
     * @param menuId
     */
    function selectGasStation ( gasStation ) {
      self.selected = angular.isNumber(gasStation) ? $scope.gasStations[gasStation] : gasStation;
    }

    /**
     * Show the Contact view in the bottom sheet
     */
    function makeContact(selectedGasStation) {

        $mdBottomSheet.show({
          controllerAs  : "cp",
          templateUrl   : './src/gasStations/view/contactSheet.html',
          controller    : [ '$mdBottomSheet', '$mdToast', ContactSheetController],
          parent        : angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
          $log.debug( clickedItem.name + ' clicked!');
        });

        /**
         * GasStation ContactSheet controller
         */
        function ContactSheetController( $mdBottomSheet, $mdToast ) {

          var innerSelf = this;

          this.gasStation = selectedGasStation;
          this.actions = [
            { name: 'Telefon'       , icon: 'phone'       , icon_url: 'assets/svg/phone.svg'},
            { name: 'E-post'     , icon: 'email' , icon_url: 'assets/svg/email.svg'}
          ];
          this.contactGasStation = function(action) {
            // The actually contact process has not been implemented...
            // so just hide the bottomSheet

            $mdBottomSheet.hide(action);
          };
          var last = {
            bottom: true,
            top: false,
            left: false,
            right: true
          };

          innerSelf.toastPosition = angular.extend({},last);

          function sanitizePosition() {
            var current = innerSelf.toastPosition;
            if ( current.bottom && last.top ) current.top = false;
            if ( current.top && last.bottom ) current.bottom = false;
            if ( current.right && last.left ) current.left = false;
            if ( current.left && last.right ) current.right = false;
            last = angular.extend({},current);
          }

          innerSelf.getToastPosition = function() {
            sanitizePosition();
            return Object.keys(innerSelf.toastPosition)
              .filter(function(pos) { return innerSelf.toastPosition[pos]; })
              .join(' ');
          };

          innerSelf.showSimpleToast = function() {
            $mdToast.show(
              $mdToast.simple()
                .textContent('Feature kommer snart!')
                .position(innerSelf.getToastPosition())
                .hideDelay(3000)
            );
          };
        }
    }

    self.simulateQuery = false;
    self.isDisabled    = false;

    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(gasStation) {
        return (angular.lowercase(gasStation.name).indexOf(lowercaseQuery) === 0);
      };
    }

    self.querySearch = function(query) {
      var results = query ? self.gasStations.filter( createFilterFor(query) ) : self.gasStations,
        deferred;
      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }
    self.searchTextChange = function(text) {
      $log.info('Text changed to ' + text);
    }
    self.selectedItemChange = function(item) {
      $log.info('Item changed to ' + JSON.stringify(item));
    }

  }

})();
